FROM nixos/nix

RUN nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
RUN nix-channel --update

RUN nix-env -f "<nixpkgs>" -i -E "pkgs: (pkgs {config.sc2-headless.accept_license = true; config.allowUnfree = true;}).sc2-headless" && nix-collect-garbage -d
RUN nix-env -f "<nixpkgs>" -iA stack git protobuf zlib 
RUN git clone https://www.gitlab.com/spacekitteh/sc2hs.git && cd sc2hs && stack build --nix --only-dependencies --no-haddock-deps --nix-add-gc-roots && cd .. && rm -rf sc2hs
RUN mkdir ~/StarcraftII && echo "executable = $(find $(nix-env -q --out-path --no-name sc2-headless) -name SC2_x64)" > ~/StarcraftII/ExecuteInfo.txt