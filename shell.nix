{ghc, runSC2 ? true}:
with (import <nixpkgs> {config.sc2-headless.accept_license = true; config.allowUnfree = true;});

haskell.lib.buildStackProject {
  inherit ghc;
  name = "sc2hsEnv";
  buildInputs = [ protobuf zlib pkgconfig llvm git curl] ++ lib.optionals runSC2 [sc2-headless];
}