{-# LANGUAGE CPP #-}
module Network.SC2.Internal.Directories where

import System.IO
import System.Directory
import System.FilePath
import qualified Data.Text as T
import qualified Data.Text.IO as T

userSC2SubDirectory :: FilePath
#ifdef mingw32_HOST_OS
userSC2SubDirectory = "Documents" </> "Starcraft II"
supportSubdir = "Support64"
#else
userSC2SubDirectory = "StarcraftII"
supportSubdir = ""
#endif

data SC2Paths = Paths
  {
    gameExecutable :: FilePath,
    supportDir :: FilePath,
    baseDir :: FilePath

  } deriving (Show)


makeSC2Paths :: Maybe FilePath -> IO SC2Paths
makeSC2Paths (Just ex) = return (makeSC2PathsFromExecutable ex)
makeSC2Paths Nothing = do
  homeDir <- getHomeDirectory
  let configFile = homeDir </> userSC2SubDirectory </> "ExecuteInfo.txt"
  theText <- T.readFile configFile
  let contents = head . lines . T.unpack $ theText
  let exe = drop 13 contents
  return (makeSC2PathsFromExecutable exe)
makeSC2PathsFromExecutable :: FilePath -> SC2Paths
makeSC2PathsFromExecutable ex = Paths {gameExecutable = ex, supportDir = support, baseDir = base } where 
  base = (takeDirectory . takeDirectory . takeDirectory)  ex
  support = base </> supportSubdir -- TODO: Test for x86 vs x64