{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE NoMonadFailDesugaring      #-}
{-# LANGUAGE OverloadedLabels           #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PartialTypeSignatures      #-}
{-# LANGUAGE PatternSynonyms            #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module Network.SC2.Agent where

import           Control.Effects.Logging
import           Control.Lens hiding ((^..))
import           Control.Lens.TH
import           Control.Monad.Freer
import           Control.Monad.Freer.Reader
import           Control.Monad.Freer.State
import           Control.Monad.Freer.TH
import           Data.Coerce
import           Data.Foldable
import           Data.Functor.Identity
import           Data.Maybe
import           Data.OpenUnion                 ( (:++:) )
import           Data.ProtoLens                 ( defMessage )
import qualified Data.Text                     as T
import           Data.Traversable
import           Data.Word

import qualified Network.SC2.Constants.Abilities
                                               as A
import           Network.SC2.Constants.Entities
import qualified Network.SC2.Constants.Units   as U
import           Network.SC2.LowLevel.Convert
import           Network.SC2.LowLevel.Protocol as LLP
                                                ( SC2Control
                                                , Split
                                                , getStatus
                                                , syncRequest
                                                , unsafeRequest
                                                , unsafeResponse
                                                )
import           Network.SC2.LowLevel.Requests as LLR
import           Network.SC2.LowLevel.Types
import           Network.SC2.LowLevel.Unit     as Unit
import qualified Proto.S2clientprotocol.Raw    as S
import qualified Proto.S2clientprotocol.Sc2api as S

import qualified Data.Vector.Generic as VG
import qualified Data.Vector as V
import Data.Vector.Generic.Lens (toVectorOf)
import qualified Data.Map as Map


type IndexedUnitContainer a = Map.Map UnitKey a

--todo: algebraic-graphs
data AgentUnitKnowledge = AgentUnitKnowledge {
    _myUnits :: IndexedUnitContainer Unit,
    _minerals :: IndexedUnitContainer MineralPatch,
    _buildings :: IndexedUnitContainer Building,
    _geysers :: IndexedUnitContainer Geyser,
    _collapsableTerrain :: IndexedUnitContainer CollapsableTerrain,
    _destroyableTerrain :: IndexedUnitContainer DestroyableTerrain,
    _enemyUnits :: IndexedUnitContainer EnemyUnit,
    _xelNagaTowers :: IndexedUnitContainer XelNagaTower,
    _neutralUnits :: IndexedUnitContainer NeutralUnit,
    _alliedUnits :: IndexedUnitContainer AlliedUnit
} deriving (Eq, Show)
makeFieldsNoPrefix ''AgentUnitKnowledge
emptyAgentUnitKnowledge = AgentUnitKnowledge Map.empty Map.empty Map.empty Map.empty Map.empty Map.empty Map.empty Map.empty Map.empty Map.empty
data WorldState = WorldState {
                    _gameInfo           :: GameInfoResponse,
                    _currentObservation :: !S.ResponseObservation,
                    _knownUnits :: !AgentUnitKnowledge

                    } deriving (Eq, Show)
makeFieldsNoPrefix ''WorldState

-- | Higher-level actions a bot can do.
infixl 8 ^..
(^..) = flip toVectorOf


data Agent a where
    UpdateObservations ::Agent () -- ^ Update the observations. Should be used at least once per step.
    ViewPlayerResources ::Agent PlayerResources -- ^ View your resources
    ViewMapInfo ::Agent MapInfo -- ^ View the map information
    ViewKnownUnits ::Agent UnitContainer -- ^ View all current known units directly observable
    OrderUnits :: Orderable a=> a -> IssueOrder -> Agent () -- ^ Issue an order to some `Orderable` unit or unit group
    SendChat ::T.Text -> ChatChannel -> Agent () -- ^ Send a message in chat
    ViewWorldState ::Agent WorldState -- ^ View all known information about the world
    Step ::Agent () -- ^ Signal that you are ready to advance to the next game tick. In the normal /fast/ mode, there are 22.4 ticks per second.
    StepN ::Word -> Agent () -- ^ Signal that you are ready to advance /n/ ticks.
    GetStatus ::Agent S.Status -- ^ Get the status of the protocol state machine.
    ViewAbilities :: (IdentifiesUnit a) => a -> Agent (V.Vector A.AbilityType)


$(makeEffect ''Agent)

type SC2Agent r = Member Agent r
type SC2AgentEffects = Agent
type SC2LogCategories = Int
type SC2LogMessages = T.Text
type SC2Logging = Logging SC2LogCategories SC2LogMessages
type SC2AgentSupportEffects = '[Split, SC2Logging]
runSC2Agent
    :: (Members SC2AgentSupportEffects r)
    => WorldState
    -> Eff (Agent ': r) ~> Eff ( SC2Control ': r)
runSC2Agent initialState = evalState initialState . reinterpret2 act  where
    act
        :: (Members SC2AgentSupportEffects r)
        => Agent  ~> Eff (State WorldState ': SC2Control ': r)
    act Network.SC2.Agent.Step = do --ugh
        result <- syncRequest LLR.Step --ugh

        case result of
            Right () -> return ()
            Left  e  -> logError ("runSC2Agent Step: " `T.append` e)
    act (StepN i) = do
        result <- syncRequest (StepMany i)
        case result of
            Right () -> return ()
            Left  e  -> logError ("runSC2Agent StepN: " `T.append` e)

    act GetStatus          = LLP.getStatus

    act UpdateObservations = do
        Right obs <- syncRequest ((defMessage & #observation .~ defMessage) :: S.Request) -- TODO: Requestable

        currState <- get @WorldState
        put $ processObservations currState (obs ^. #observation)

    act ViewKnownUnits = do
        state <- get @WorldState
        return
            $  state
            ^. currentObservation
            .  #observation
            .  #rawData
            .  #vec'units
            &  each
            %~ (fromJust . convertFrom)

    act ViewWorldState           = get
    act (OrderUnits units order) = do -- TODO: This is terribly hacky. Make Action a Requestable!

        let aruc =
                (defMessage :: S.ActionRawUnitCommand)
                    &  #abilityId
                    .~ A.toInt (order ^. abilityID)
                    &  setTarget (order ^. target)
                    &  #vec'unitTags
                    .~ toTags units
        let ar = (defMessage :: S.ActionRaw) & #unitCommand .~ aruc
        let a =
                (defMessage :: S.RequestAction)
                    &  #actions
                    .~ [(defMessage :: S.Action) & #actionRaw .~ ar]
        _ <- syncRequest ((defMessage & #action .~ a) :: S.Request)
        return ()
      where
        setTarget (TargetPoint p) m = m & #targetWorldSpacePos .~ convertTo p
        setTarget (TargetUnit  u) m = m & #targetUnitTag .~ coerce u




enemyStartLocations :: Eff (Agent ': r) [Point]
enemyStartLocations = do
    state <- viewWorldState
    return $ (startLocations . fromJust . startRaw) $ state ^. gameInfo
processObservations :: WorldState -> S.ResponseObservation -> WorldState
processObservations state obs = state & currentObservation .~ obs  where



--TODO: Turn this into a lens (is it a zoom?) over arbitrary UnitContainer

selectFromAllUnits :: SC2Agent r => (Unit -> Bool) -> Eff r UnitContainer
selectFromAllUnits f = VG.filter f <$> viewKnownUnits


type UnitFilter = forall p f . (Choice p, Applicative f) => Optic' p f Unit Unit
{-#INLINABLE mine#-}
-- | A lens-like filter on units. As this uses `filtered`, you must be sure not to invalidate the filter predicate when using it.
mine :: UnitFilter
mine = filtered (\u -> u ^. alliance == S.Self)

units :: SC2Agent r => Eff r UnitContainer
units = do
    knownUnits <- viewKnownUnits
    return (knownUnits ^.. traversed . mine) -- .folded. my)







workers :: SC2Agent r => Eff r UnitContainer
workers = do
    un <- units
    return (VG.filter isWorker un)-- isWorker un)





{-#INLINABLE isWorker' #-}
isWorker' :: U.UnitType -> Bool
isWorker' u = u == U.Probe || u == U.SCV || u == U.Drone
{-#INLINABLE isWorker #-}
isWorker :: Unit.Unit -> Bool
isWorker u = u ^. unitType & isWorker'
{-#INLINABLE isHarvesterUnit' #-}
isHarvesterUnit' :: U.UnitType -> Bool
isHarvesterUnit' u = isWorker' u || u == U.MULE
{-#INLINABLE isHarvesterUnit #-}
isHarvesterUnit :: Unit.Unit -> Bool
isHarvesterUnit u = u ^. unitType & isHarvesterUnit'

viewUnitsOfPlayer :: SC2Agent r => PlayerID -> Eff (r) UnitContainer
viewUnitsOfPlayer player = selectFromAllUnits (\u -> u ^. owner == player)


initialiseWithGameInfo :: GameInfoResponse -> WorldState
initialiseWithGameInfo gi = WorldState gi defMessage emptyAgentUnitKnowledge


attack :: (Member Agent r, Orderable a, Targetable b) => a -> b -> Eff r ()
attack units target = do
    send $ OrderUnits units order
    where order = IssueOrder A.Attack (asTarget target) False
