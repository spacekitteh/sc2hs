{-#LANGUAGE GADTs, TypeOperators, FlexibleContexts, DataKinds, RankNTypes, TypeApplications, ScopedTypeVariables, AllowAmbiguousTypes #-}
module Control.Effects.Logging where

import Control.Monad.Freer
import Data.Text
import Data.String

-- | Logging effect.
data Logging categories message a where
    LogMessage :: (Enum c, Show message) => c -> message -> Logging c m () -- ^ Log a `message` at a priority level `c`



logMessage :: (Member (Logging Int Text) effs) => Int -- ^ The logging level. Lower = more important. Higher = more verbose.
                                    -> Text-> Eff effs ()
logMessage logLevel msg = send ((LogMessage logLevel   msg) :: Logging Int Text ())


runSimpleLoggingStdOut :: (Enum c, Show m, Member IO r) => Int -> Eff ((Logging c m) ': r) ~> Eff r
runSimpleLoggingStdOut maxLevel = interpret act where
    act :: (Member IO r) => (Logging c m) ~> Eff r
    act (LogMessage lvl msg) = do
        if (fromEnum lvl) <= maxLevel then send (putStrLn.show $ msg) else return ()

logError ::  (Member (Logging Int Text) effs) => Text -> Eff effs ()
logError = logMessage 0