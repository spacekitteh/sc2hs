{-#LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-#LANGUAGE OverloadedLabels #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TypeApplications #-}
module Network.SC2.LowLevel.Requests
       ( module Network.SC2.LowLevel.Types
       , Ping(..)
       , PingResponse(..)
       , Fog(..)
       , Realtime(..)
       , Seed(..)
       , CreateGame(..)
       , JoinGame(..)
       , RestartGame(..)
       -- , StartReplay(..)
       , LeaveGame(..)
       , QuickSave(..)
       , QuickLoad(..)
       , QuitGame(..)
       , GameInfo(..)
       , GameInfoResponse(..)
       , Step(..)
       , AvailableMaps(..)
       ) where

import qualified Proto.S2clientprotocol.Common as C
import qualified Proto.S2clientprotocol.Common_Fields as C
import qualified Proto.S2clientprotocol.Sc2api as A
import qualified Proto.S2clientprotocol.Raw as R
import qualified Proto.S2clientprotocol.Raw_Fields as R
import qualified Proto.S2clientprotocol.Query as Q
import qualified Proto.S2clientprotocol.Query_Fields as Q
import qualified Proto.S2clientprotocol.Error as E
import Control.Lens
import Control.Lens.TH
import Control.Monad
import Data.Coerce
import Data.Default.Class
import Data.Maybe (fromMaybe, fromJust)
import qualified Data.Text as T
import Data.Void
import Network.SC2.LowLevel.Requestable
import Network.SC2.LowLevel.Types
import Network.SC2.LowLevel.Convert
import qualified Network.SC2.Constants.Abilities as AB
import qualified Network.SC2.Constants.Units as UN
import Data.ProtoLens.Labels
import qualified Data.Vector.Generic as VG
import qualified Data.Vector as V
import Data.Vector.Generic.Lens (toVectorOf)
import Data.ProtoLens (defMessage)

data Ping = Ping
          deriving (Show, Eq)

data PingResponse =
  PingResponse
  { gameVersion :: T.Text
  , dataVersion :: T.Text
  , dataBuild :: Int
  , baseBuild :: Int
  } deriving (Show, Eq)

instance Requestable Ping where
  type ResponseOf Ping = PingResponse
  toRequest _ = defMessage& #ping .~ defMessage
  fromResponse _ = extractResponse (view #maybe'ping >=> convert)
    where convert r = PingResponse <$> r^. #maybe'gameVersion <*> r^. #maybe'dataVersion <*> (fromIntegral <$> r^. #maybe'dataBuild ) <*> (fromIntegral <$> r^. #maybe'baseBuild)


data Fog = Fog | NoFog
         deriving (Show, Eq, Enum)

data Realtime = Stepped | Realtime
              deriving (Show, Eq, Enum)

data Seed = Seed Int | RandomSeed
          deriving (Show, Eq)

data CreateGame = CreateGame Map [PlayerType Race]
                | CreateGameFull Map [PlayerType Race] Fog Seed Realtime
                deriving (Show, Eq)

instance Requestable CreateGame where
  type ResponseOf CreateGame = ()
  toRequest (CreateGame map players) = toRequest (CreateGameFull map players Fog RandomSeed Stepped)
  toRequest (CreateGameFull map players fog seed rt) = defMessage& #createGame .~ mods defMessage
    where
      mods = mapmod map . playermod . fogmod . seedmod seed . rtmod

      fogmod :: A.RequestCreateGame -> A.RequestCreateGame
      fogmod = #disableFog .~ (fog == NoFog)

      seedmod :: Seed -> A.RequestCreateGame -> A.RequestCreateGame
      seedmod RandomSeed = id
      seedmod (Seed s) = #randomSeed .~ fromIntegral s

      rtmod :: A.RequestCreateGame -> A.RequestCreateGame
      rtmod = #realtime .~ (rt == Realtime)

      mapmod :: Map -> A.RequestCreateGame -> A.RequestCreateGame
      mapmod (BattlenetMap m) = #battlenetMapName .~ m
      mapmod (LocalMap m d) = #localMap .~ (defMessage & #mapPath .~ m & #maybe'mapData .~ d)

      playermod :: A.RequestCreateGame -> A.RequestCreateGame
      playermod = #playerSetup .~ fmap convertPlayerType players
  fromResponse _ = void . extractResponseErr (view #maybe'createGame) Just (view #maybe'errorDetails)



-- FIXME as observer? ports? render / feature layer interface?
data JoinGame = JoinGame PlayerName Race [Interface Void]
              deriving (Show, Eq)

instance Requestable JoinGame where
  type ResponseOf JoinGame = PlayerID
  toRequest (JoinGame name r ifaces) = defMessage& #joinGame .~ mod defMessage
    where
      mod m = m & #playerName .~ name & racemod . foldr (.) id (fmap ifacemod ifaces)

      racemod :: A.RequestJoinGame -> A.RequestJoinGame
      racemod = #race .~ convertRace r

      ifacemod :: Interface Void -> A.RequestJoinGame -> A.RequestJoinGame
      ifacemod Raw = #options . #raw .~ True
      ifacemod Score = #options . #score .~ True
  fromResponse _ = fmap (PlayerID . fromIntegral) . extractResponseErr (view #maybe'joinGame) (view #maybe'playerId) (view #maybe'errorDetails)

data RestartGame = RestartGame
                 deriving (Show, Eq)


instance Requestable RestartGame where
  type ResponseOf RestartGame = ()
  toRequest _ = defMessage& #restartGame .~ defMessage
  fromResponse _ = void . extractResponseErr (view #maybe'restartGame) Just (view #maybe'errorDetails)

-- StartReplay

data LeaveGame = LeaveGame
               deriving (Show, Eq)

instance Requestable LeaveGame where
  type ResponseOf LeaveGame = ()
  toRequest _ = defMessage& #leaveGame .~ defMessage
  fromResponse _ = void . extractResponse (view #maybe'leaveGame)

data QuickSave = QuickSave
               deriving (Show, Eq)

instance Requestable QuickSave where
  type ResponseOf QuickSave = ()
  toRequest _ = defMessage& #quickSave .~ defMessage
  fromResponse _ = void . extractResponse (view #maybe'quickSave)

data QuickLoad = QuickLoad
               deriving (Show, Eq)

instance Requestable QuickLoad where
  type ResponseOf QuickLoad = ()
  toRequest _ = defMessage& #quickLoad .~ defMessage
  fromResponse _ = void . extractResponse (view #maybe'quickLoad)

data QuitGame = QuitGame
          deriving (Show, Eq)

instance Requestable QuitGame where
  type ResponseOf QuitGame = ()
  toRequest _ = defMessage& #quit .~ defMessage
  fromResponse _ = void . extractResponse (view #maybe'quit)



instance Requestable GameInfo where
  type ResponseOf GameInfo = GameInfoResponse
  toRequest _ = defMessage& #gameInfo .~ defMessage
  fromResponse _ = extractResponse (view #maybe'gameInfo >=> convert)
    where
      convert gi = do
        mname <- gi^. #maybe'mapName
        let mods = gi^. #modNames
        localpath <- T.unpack <$> (gi ^. #maybe'localMapPath)
        players <- traverse convertPlayer (gi^. #playerInfo) -- TODO: Lensify this whole function
        let raw = convertRaw =<< gi ^. #maybe'startRaw
        let ifaces = (snd . iface #maybe'raw id Raw . iface #maybe'score id Score . iface #maybe'featureLayer (const True) (FeatureLayer ()) . iface #maybe'render (const True) (Render ())) (gi, [])
        return (GameInfoResponse mname mods localpath players raw ifaces)
      iface :: Lens' A.InterfaceOptions (Maybe b) -> (b -> Bool) -> a -> (A.ResponseGameInfo, [a]) -> (A.ResponseGameInfo, [a])
      iface l t x (gi, xs) = case gi ^. #options . l of
        Just b | t b -> (gi, x : xs)
        _            -> (gi, xs)
      convertPlayer p =
        let playerName = fromMaybe "Unnamed" (p ^. #maybe'playerName)
        in do
        pid <- PlayerID . fromIntegral <$> (p ^. #maybe'playerId)
        typ <- p ^. #maybe'type'
        ourtyp <- case typ of
          A.Observer -> return Observer
          A.Participant -> do
            race <- convertRace p
            return (Participant race)
          A.Computer -> do
            race <- convertRace p
            diff <- p ^. #maybe'difficulty
            return (Computer race diff)
        return (PlayerInfo pid ourtyp playerName)
      convertRace p = do
        req <-  p ^. #maybe'raceRequested
        let act =  p ^. #maybe'raceActual
        return (convertRaceBack req (flip convertRaceBack (error "convertRace") <$> act))
      convertRaw :: R.StartRaw -> Maybe StartRaw
      convertRaw r = do
        msize <- convertFrom =<< (r^. #maybe'mapSize)
        pagrid <- extractTypedImage r #maybe'pathingGrid
        theight <- extractTypedImage r #maybe'terrainHeight
        plgrid <- extractTypedImage r #maybe'placementGrid
        parea <- convertFrom =<< (r^. #maybe'playableArea)
        starts <- traverse convertFrom (r^. #startLocations)
        return (StartRaw msize pagrid theight plgrid parea starts)

-- FIXME RequestObservation

-- FIXME RequestAction

data Step = Step
          | StepMany !Word

instance Requestable Step where
  type ResponseOf Step = ()
  toRequest !Step = toRequest (StepMany 1)
  toRequest !(StepMany !i) = defMessage& #step . #count .~ fromIntegral i
  fromResponse _ = void . extractResponse (view #maybe'step)

data AvailableMaps = AvailableMaps
                   deriving (Show, Eq)

instance Requestable AvailableMaps where
  type ResponseOf AvailableMaps = [Map]
  toRequest _ = defMessage& #availableMaps .~ defMessage
  fromResponse _ = extractResponse (fmap makeMaps . view #maybe'availableMaps) -- TODO: Lensify fmap
    where makeMaps :: A.ResponseAvailableMaps -> [Map]
          makeMaps m = (LocalMap  <$> m ^. #localMapPaths <*> pure Nothing)
                       ++ (BattlenetMap  <$> m ^. #battlenetMapNames)


data PathingQuery = PathingQuery {_start :: Target, _destination :: Point} deriving (Eq, Show)
makeFieldsNoPrefix ''PathingQuery
data PathingQueryResponse = PathLength {_distance :: Distance}
                            | NoPathExists deriving (Eq, Show)
makeFieldsNoPrefix ''PathingQueryResponse

instance ConvertProto Q.RequestQueryPathing where
  type Unproto Q.RequestQueryPathing = PathingQuery
  convertTo (PathingQuery (TargetPoint p) d) = defMessage & #startPos .~ (convertTo p) & #endPos .~ convertTo d
  convertTo (PathingQuery (TargetUnit u) d) = defMessage & #unitTag .~ (unRawTag u) & #endPos .~ convertTo d
  convertFrom _ = error "ConvertProto RequestQueryPathing"

instance ConvertProto Q.ResponseQueryPathing where
  type Unproto Q.ResponseQueryPathing = PathingQueryResponse
  convertTo _ = error "ConvertProto ResponseQueryPathing"
  convertFrom r | r ^. #distance == 0 = Just NoPathExists
                | otherwise = Just (PathLength (r ^. #distance))

newtype AvailableAbilitiesQuery = AvailableAbilitiesQuery { _theUnit :: UnitID} deriving (Eq, Show)
makeFieldsNoPrefix ''AvailableAbilitiesQuery

instance ConvertProto Q.RequestQueryAvailableAbilities where
  type Unproto Q.RequestQueryAvailableAbilities = AvailableAbilitiesQuery
  convertTo q = defMessage & #unitTag .~ coerce q
  convertFrom _ = error "ConvertProto Q.RequestQueryAvailableAbilities"

data AvailableAbility = AvailableAbility {_ability :: AB.AbilityType, _requiresPoint :: Bool} deriving (Eq, Show)
makeFieldsNoPrefix ''AvailableAbility

instance ConvertProto C.AvailableAbility where
  type Unproto C.AvailableAbility = AvailableAbility
  convertFrom m = Just (AvailableAbility (AB.fromInt (m ^. #abilityId)) (m ^. #requiresPoint))
  convertTo _ = error "ConvertProto C.AvailableAbility"


data AvailableAbilities = AvailableAbilities { _abilities :: V.Vector AvailableAbility, _unitTag :: UnitID, _unitType :: UN.UnitType } deriving (Eq, Show)
makeFieldsNoPrefix ''AvailableAbilities
instance ConvertProto Q.ResponseQueryAvailableAbilities where
  type Unproto Q.ResponseQueryAvailableAbilities = AvailableAbilities
  convertFrom m = Just $ AvailableAbilities (VG.map (fromJust . convertFrom @C.AvailableAbility) (m ^. #vec'abilities)) (UnitID (m ^. #unitTag)) (UN.fromInt (m ^. #unitTypeId))
  convertTo _ = error "ConvertProto ResponseQueryAvailableAbilities "


data BuildingPlacementQuery = BuildingPlacementQuery {_ability :: AB.AbilityType, _targetPosition :: Point,
                                _placingUnitTag :: Maybe UnitID} deriving (Eq, Show)
makeFieldsNoPrefix ''BuildingPlacementQuery

instance ConvertProto Q.RequestQueryBuildingPlacement where
  type Unproto Q.RequestQueryBuildingPlacement = BuildingPlacementQuery
  convertTo (BuildingPlacementQuery ab pos u) = defMessage & #abilityId .~ (AB.toInt ab) & #targetPos .~ (convertTo pos)
                                                & #maybe'placingUnitTag .~ (fmap coerce u)
  convertFrom _ = error "ConvertProto BuildingPlacementQuery"

data Queries = Queries {_pathing :: V.Vector PathingQuery, _abilities :: V.Vector AvailableAbilitiesQuery,
                        _placements :: V.Vector BuildingPlacementQuery, _ignoreResourceRequirements :: Bool } deriving (Eq, Show)
makeFieldsNoPrefix ''Queries

data QueryAnswers = QueryAnswers {
                    _pathing :: V.Vector PathingQueryResponse,
                    _abililties :: V.Vector AvailableAbilities,
                    _placements :: V.Vector E.ActionResult
} deriving (Eq, Show)
makeFieldsNoPrefix ''QueryAnswers
instance Requestable Queries where
  type ResponseOf Queries = QueryAnswers
  toRequest q = defMessage & #query . #vec'pathing .~ VG.map convertTo (q ^. pathing)
                & #query . #vec'placements .~ VG.map convertTo  (q ^. placements)
                & #query . #vec'abilities .~ VG.map convertTo (q ^. abilities)
                & #query . #ignoreResourceRequirements .~ (q ^. ignoreResourceRequirements)
  fromResponse _ = extractResponse convert where
    convert :: A.Response -> Maybe QueryAnswers
    convert res = Just (QueryAnswers pathing abilities placements) where
      r = res ^. #query
      pathing = VG.map (fromJust . convertFrom) (r ^. #vec'pathing)
      abilities = VG.map (fromJust . convertFrom) (r ^. #vec'abilities)
      placements = VG.map (view #result) (r ^. #vec'placements)
