{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedLabels #-}
module Network.SC2.LowLevel.Convert where

--import Network.SC2.LowLevel.Types
import qualified Proto.S2clientprotocol.Common as C
import qualified Proto.S2clientprotocol.Sc2api as A
import qualified Proto.S2clientprotocol.Raw as R
import Data.ProtoLens (defMessage)
import qualified Network.SC2.Constants.Units as Units
import Control.Lens
import Data.Coerce



-- | A type which can be converted to and from a protobuf message.
class ConvertProto a where
  type Unproto a
  convertTo :: Unproto a -> a
  convertFrom :: a -> Maybe (Unproto a)

  convertProto :: Prism' a (Unproto a)
  convertProto = prism' convertTo convertFrom