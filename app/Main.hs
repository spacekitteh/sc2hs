{-#LANGUAGE NoMonadFailDesugaring #-}
{-#LANGUAGE OverloadedLabels, TypeOperators, DataKinds, MonoLocalBinds #-}
{-#LANGUAGE OverloadedStrings #-}
{-#LANGUAGE FlexibleContexts #-}
module Main where

import Network.SC2.LowLevel hiding (getStatus, Step)
import qualified Proto.S2clientprotocol.Sc2api as A
import qualified Proto.S2clientprotocol.Common as A
import qualified Proto.S2clientprotocol.Sc2api_Fields as A
import qualified Proto.S2clientprotocol.Raw as A
import qualified Proto.S2clientprotocol.Raw_Fields as A

import Data.ProtoLens.Labels
import Data.Maybe
import Control.Lens
import Control.Effects.Logging
import Network.SC2.Agent
import Data.Text hiding (head)
import System.Directory
import System.FilePath
import Data.Array.Accelerate as Accel hiding (Eq, fromIntegral, Enum, Bounded, (!!))

main :: IO ()
main = withMain mempty (runLocal  (runSimpleLoggingStdOut 999 . runSC2 bot) ) where


bot :: SC2LowLevel '[SC2Logging] ()
bot = do
  Right info <- syncRequest Ping
  logMessage 1 ("info: " `append`  (pack.show $info))

  Right maps@(map : _) <- syncRequest AvailableMaps

  Right () <- syncRequest $ CreateGameFull (maps !! 7) [Participant Protoss, Computer (Random ()) VeryEasy] Fog RandomSeed Realtime
  logMessage 1 "create game success"

  Right pid <- syncRequest $ JoinGame "StarkRaftWho" Protoss [Raw]
  logMessage 1 ("join game: " `append`  (pack.show $ pid))

  Right info <- syncRequest  GameInfo
  send $ do
    homeDir <- getHomeDirectory
    createDirectoryIfMissing True (homeDir </>"sc2")
    saveImageGreyscale (homeDir </>"sc2" </>"height.bmp") (terrainHeight . fromJust . startRaw $ info)
  logMessage 5 (pack.show $ info)

  runSC2Agent (initialiseWithGameInfo info) workerRush



-- Orders a worker rush on the first frame.
workerRush :: Member SC2Logging r =>   Eff (Agent ':r ) ()
workerRush = do
  updateObservations
  units <- workers
  locs <- enemyStartLocations
  units `attack` head locs
  logMessage 1 "Worker rush ^_^"
  loop

loop :: Member SC2Logging r =>  Eff (Agent ': r) ()
loop = do
  step
  status <- getStatus
  case status of
    Ended -> logMessage 1 "Ending game"
    _     -> loop
