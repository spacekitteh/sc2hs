{-#LANGUAGE TemplateHaskell #-}

module Network.SC2.ProtocolModel where

import qualified Development.GitRev as G

branch :: String
branch = $(G.gitBranch)

hash :: String
hash = $(G.gitHash)

commitDate :: String
commitDate = $(G.gitCommitDate)

